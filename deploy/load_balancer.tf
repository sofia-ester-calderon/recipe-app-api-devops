resource "aws_lb" "api" {
  name = "${local.prefix}-main"
  # there two load balancers types
  # - application lb: handle requests at the HTTP level (http & https)
  # - network lb: handle requests at the network level (tco & udp)
  load_balancer_type = "application"
  # public subnet to be accessible from the internet
  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id
  ]

  security_groups = [aws_security_group.lb.id]

  tags = local.common_tags
}

# target group is the group of servers that the load balancer
# can forward requests to
resource "aws_lb_target_group" "api" {
  name     = "${local.prefix}-api"
  protocol = "HTTP"
  # load balancer target group in the same VPC as our applocation
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
  # port proxy will run on in ECS task
  port = 8000

  # health check is a feature of lb which allow them to perform
  # regular polls on our application to ensure it's running
  health_check {
    path = "/admin/login/"
  }
}

# accepts the request to our load balancer
# entry point into our lb
resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.api.arn
  # port that the listener will accept the requests on
  port     = 80
  protocol = "HTTP"

  # redirect any request to http listener to our https
  default_action {
    type = "redirect"

    redirect {
      port     = "443"
      protocol = "HTTPS"
      # default status code for http redirect response
      status_code = "HTTP_301"
    }
  }
}

# listener to handle requests to our lb using https
resource "aws_lb_listener" "api_https" {
  load_balancer_arn = aws_lb.api.arn
  port              = 443
  protocol          = "HTTPS"

  certificate_arn = aws_acm_certificate_validation.cert.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }
}

# allows access into our load balancer
resource "aws_security_group" "lb" {
  description = "allow access to application load balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.main.id

  # accepts all connections from the public internet into 
  # our load balancer
  # into our lb on port 80
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow requests to our load balancer from https
  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  # access our load balancer has to our application
  # out of our lb on port 8000
  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}