########################
# Create cluster
########################

resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

########################
# Create ecs roles
########################

# whenever creating an ecs task, these two roles have to be defined

# creates new IAM policy in AWS account
# task execution role: a role that is used for starting a service
# AWS resources that id needs to access in order to start the service

# permissions in json:
#       - retrieve the image from the Docker repository
#       - create and put logs into the log stream
resource "aws_iam_policy" "task_execution_role_policy" {
  name = "${local.prefix}-task-exec-role-policy"
  # set policy to root path
  path        = "/"
  description = "Allow retrieving retrieving of images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json")
}

# create an IAM role and attach the policy to that role
resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

# give permissions to our task that it needs at runtime
# permissions our docker container needs after it started
resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}


################################################################################
# Log group: to group all of the logs for our particular task into one place
################################################################################

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"

  tags = local.common_tags
}

###########################################################
# Define our task definition within our ECS terraform
###########################################################

# Container definition template: a json file which container 
# all the details of our container so AWS knows how to run it in production
# e.g.
#   - image regsitry and image tag
#   - memory to assign to it
#   - different configuration options (i.e. environment variables)

data "template_file" "api_container_definitions" {
  template = file("./templates/ecs/container-definitions.json.tpl")

  vars = {
    app_image         = var.ecr_image_api
    proxy_image       = var.ecr_image_proxy
    django_secret_key = var.django_secret_key
    db_host           = aws_db_instance.main.address
    db_name           = aws_db_instance.main.name
    db_user           = aws_db_instance.main.username
    db_pass           = aws_db_instance.main.password
    log_group_name    = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region  = data.aws_region.current.name
    # set allowd host to our custom created dns
    allowed_hosts            = aws_route53_record.app.fqdn
    s3_storage_bucket_name   = aws_s3_bucket.app_public_files.bucket
    s3_storage_bucket_region = data.aws_region.current.name
  }
}

resource "aws_ecs_task_definition" "api" {
  family = "${local.prefix}-api"
  # rendered means it has passed all the vars defined above
  container_definitions = data.template_file.api_container_definitions.rendered
  # fargate type of ecs hosting - serverless version of deploying containers
  requires_compatibilities = ["FARGATE"]
  # we want to use our containers with our vpc so they can connect to our databse through 
  # the vpc network that we created
  network_mode = "awsvpc"
  cpu          = 256
  memory       = 512
  # gives the permissions to execute a new task
  execution_role_arn = aws_iam_role.task_execution_role.arn
  # role that is given to the actual running task
  task_role_arn = aws_iam_role.app_iam_role.arn

  volume {
    name = "static"
  }

  tags = local.common_tags
}
resource "aws_security_group" "ecs_service" {
  description = "access for the ECS service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  # allow outbound access from our container on port 443 - https
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow outbound access from our service on port 5432 - our db
  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
  }

  # accept inbound connection from lb security group to proxy
  ingress {
    from_port = 8000
    to_port   = 8000
    protocol  = "tcp"
    security_groups = [
      aws_security_group.lb.id
    ]
  }
  tags = local.common_tags
}

resource "aws_ecs_service" "api" {
  name            = "${local.prefix}-api"
  cluster         = aws_ecs_cluster.main.name
  task_definition = aws_ecs_task_definition.api.family
  # number of tasks we wish to run inside the service
  desired_count = 1
  # fargate allows you to run tasks without managing the servers
  launch_type = "FARGATE"

  # connections to our ecs service will go through our load balancer
  # which will forward the request to our private network
  network_configuration {
    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]
    security_groups = [aws_security_group.ecs_service.id]
  }

  # tells our ecs service to register new tasks with our target group
  load_balancer {
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = "proxy"
    container_port   = 8000
  }

  depends_on = [aws_lb_listener.api_https]
}

data "template_file" "ecs_s3_write_policy" {
  template = file("./templates/ecs/s3-write-policy.json.tpl")

  vars = {
    bucket_arn = aws_s3_bucket.app_public_files.arn
  }
}

resource "aws_iam_policy" "ecs_s3_access" {
  name        = "${local.prefix}-AppS3AccessPolicy"
  path        = "/"
  description = "allow access to the recipe app s3 bucket"

  policy = data.template_file.ecs_s3_write_policy.rendered
}

resource "aws_iam_role_policy_attachment" "ecs_s3_access" {
  role       = aws_iam_role.app_iam_role.name
  policy_arn = aws_iam_policy.ecs_s3_access.arn
}