resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id,
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

# When networking is setup, security can be set up which can be applied to resources
# Allows to control the inbound access allowed to that resource
resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  # only inbount access is setup
  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    # security group we created for bastion server
    # and for our ecs service
    security_groups = [
      aws_security_group.bastion.id,
      aws_security_group.ecs_service.id,
    ]
  }

  tags = local.common_tags
}

# create an RDS instance in AWS
resource "aws_db_instance" "main" {
  # db identifier used to access the db in the console
  identifier = "${local.prefix}-db"
  # actual db name that is created within the instance
  name = "recipe"
  # how much diskspace is given to the db - the lower the cheaper
  allocated_storage = 20
  # gp2 is the entry level storage type in AWS (cheaper!)
  storage_type   = "gp2"
  engine         = "postgres"
  engine_version = "11.4"
  # type of database server to run (memory and cpu that is assigned to the db)
  instance_class       = "db.t2.micro"
  db_subnet_group_name = aws_db_subnet_group.main.name
  password             = var.db_password
  username             = var.db_username
  # number of days backups should be maintained
  backup_retention_period = 0
  # determines if the db should be run on multiple availability zones
  multi_az            = false
  skip_final_snapshot = true
  # set security group to this db resource
  vpc_security_group_ids = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}