resource "aws_s3_bucket" "app_public_files" {
  bucket = "${local.prefix}-files"
  # acl = access control list - everyone can read the files
  acl = "public-read"
  # to easily destroy bucket with terraform
  force_destroy = true
}